package xyz.benhigh.pupperpicker

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import java.util.*

class MainActivity : AppCompatActivity() {
	// Stack to hold all of our doggos
	private val doggos = Stack<Doggo>()

	/**
	 * Create the activity, set the on-click listener and pick a doggo automatically
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		nextDoggoButton.setOnClickListener {
			pickNextDoggo()
		}

		prevDoggoButton.setOnClickListener {
			previousDoggo()
		}

		pickNextDoggo()
	}

	/**
	 * Make a call to the API to get a doggo and add it to the doggos stack
	 */
	private fun pickNextDoggo() {
		val queue = Volley.newRequestQueue(this)
		val url = "https://api.thedogapi.com/v1/images/search?size=med&format=json&has_breeds=true&order=RANDOM&page=0&limit=1"

		// API request
		val stringRequest = object: StringRequest(Request.Method.GET, url,
				Response.Listener<String> { response ->
					// Success handler - Get the breed and image URL, then set the widgets
					try {
						val responseJson = JSONArray(response)

						// Get a list of breeds for the dog in question
						val breeds = responseJson.getJSONObject(0).getJSONArray("breeds")
						val breedList = ArrayList<String>()
						for (i in 0 until breeds.length()) {
							val breed = breeds.getJSONObject(i)
							breedList.add(breed.getString("name"))
						}
						val breedsString = breedList.joinToString(separator = ", ")

						// Get the image URL
						val imgUrl = responseJson.getJSONObject(0).getString("url")

						// Create the doggo, add to the stack, and set the doggo on screen
						val doggo = Doggo(imgUrl, breedsString)
						doggos.push(doggo)

						// Enable the back button
						if (doggos.size > 1) {
							prevDoggoButton.isEnabled = true
						}

						setDoggoOnScreen(doggo)
					} catch (e: Throwable) {
						Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
					}
				},
				Response.ErrorListener { error ->
					// Error handler - Show the error in a Toast
					Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
				}) {
			override fun getHeaders(): Map<String, String> {
				// Set the API key as a custom header
				val headers = HashMap<String, String>()
				headers["x-api-key"] =BuildConfig.TheDogApiKey
				return headers
			}
		}

		// Send the request
		queue.add(stringRequest)
	}

	/**
	 * Pop the top doggo off the stack and set it as the doggo on screen, if such a doggo is in the
	 * stack. Otherwise, just a new doggo.
	 */
	private fun previousDoggo() {
		if (doggos.size - 1 > 0) {
			doggos.pop() // Pop the current doggo off the stack so we can get to the previous one
			val doggo = doggos.peek()
			setDoggoOnScreen(doggo)

			// Check if we need to disable the back button
			if (doggos.size == 1) {
				prevDoggoButton.isEnabled = false
			}
		} else {
			Toast.makeText(this, "No previous doggos to show. Click the next button for more doggos.", Toast.LENGTH_LONG).show()
		}
	}

	/**
	 * Set the on screen doggo to the passed on-screen doggo
	 */
	private fun setDoggoOnScreen(doggo: Doggo) {
		breedText.text = doggo.breed
		Picasso.get().load(doggo.url).placeholder(R.drawable.loading).into(doggoView)
	}
}
